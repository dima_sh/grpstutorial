/*
 * Copyright 2015, Google Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *
 *    * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.epam.fixedge.grpc.client;

import io.grpc.CallCredentials;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import io.grpc.stub.StreamObserver;

import com.epam.fixedge.adminprotocol.Event;
import com.epam.fixedge.adminprotocol.KVRequest;
import com.epam.fixedge.adminprotocol.KVServiceGrpc;
import com.epam.fixedge.adminprotocol.NotificationGrpc;
import com.epam.fixedge.adminprotocol.PingGrpc;
import com.epam.fixedge.adminprotocol.PingRequest;
import com.epam.fixedge.adminprotocol.PingResponse;
import com.epam.fixedge.adminprotocol.SubscriptionRequest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple client that requests a greeting from the {@link HelloWorldServer}.
 */
public class KVClient {
	private static final Logger logger = Logger.getLogger(KVClient.class.getName());

	private final ManagedChannel channel;
	private final KVServiceGrpc.KVServiceBlockingStub readProp;
	private final NotificationGrpc.NotificationStub asyncEvent;
	private final PingGrpc.PingBlockingStub pingBlockingStub;

	/**
	 * Construct client connecting to HelloWorld server at {@code host:port}.
	 */
	public KVClient(String host, int port) {
		channel = ManagedChannelBuilder.forAddress(host, port)
				// Channels are secure by default (via SSL/TLS). For the example
				// we disable TLS to avoid
				// needing certificates.
				.usePlaintext(true).build();

		pingBlockingStub = PingGrpc.newBlockingStub(channel);
		readProp = KVServiceGrpc.newBlockingStub(channel);
		asyncEvent = NotificationGrpc.newStub(channel);
		/*
		 * asyncEvent.subscribe(SubscriptionRequest.getDefaultInstance(), new
		 * StreamObserver<Event>() {
		 * 
		 * @Override public void onNext(Event arg0) { System.out.println(
		 * "The event come " + arg0);
		 * 
		 * }
		 * 
		 * @Override public void onError(Throwable arg0) { // TODO
		 * Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void onCompleted() { // TODO Auto-generated method
		 * stub
		 * 
		 * } });
		 */
		// asyncProp.ready(KVRequest request, responseObserver) {

		// }
	}

	/** Say ping to server. */
	public String ping(String id) {
		// logger.info("Will try to greet " + id + " ...");
		PingRequest request = PingRequest.newBuilder().setRequesId(id).build();
		PingResponse response;
		try {
			response = pingBlockingStub.ping(request);

		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			throw e;
		}
		return response.getResponseId();
	}

	/** find property in server. */
	public void lookup(String wildcard) {
		com.epam.fixedge.adminprotocol.KVRequest request = com.epam.fixedge.adminprotocol.KVRequest.newBuilder()
				.setName(wildcard).build();
		com.epam.fixedge.adminprotocol.KVList response;
		try {
			response = readProp.lookup(request);

		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			return;
		}
		System.out.println(response);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/** get property from server. */
	public void readProperty(String path) {
		// logger.info("Will try to get property " + path + " ...");
		com.epam.fixedge.adminprotocol.KVRequest request = com.epam.fixedge.adminprotocol.KVRequest.newBuilder()
				.setName(path).build();
		com.epam.fixedge.adminprotocol.KV response;
		try {
			response = readProp.read(request);

		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			return;
		}
		System.out.println(response);
	}

	/** get children in server. */
	public void children(String path) {
		// logger.info("Will try to get property " + path + " ...");
		com.epam.fixedge.adminprotocol.KVRequest request = com.epam.fixedge.adminprotocol.KVRequest.newBuilder()
				.setName(path).build();
		com.epam.fixedge.adminprotocol.KVList response;
		try {
			response = readProp.children(request);

		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			return;
		}
		System.out.println(response);
	}

	public static final String EXIT_COMMAND = "exit";

	/**
	 * Greet server. If provided, the first element of {@code args} is the name
	 * to use in the greeting.
	 */
	public static void main(String[] args) throws Exception {
		KVClient client = new KVClient("localhost", 50051);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter read , update, delete, create, children, lookup , or '" + EXIT_COMMAND + "' to quit");
		try {
			while (true) {

				System.out.print("> ");
				String input = br.readLine();
				// System.out.println(input);

				if (input.length() == EXIT_COMMAND.length() && input.toLowerCase().equals(EXIT_COMMAND)) {
					System.out.println("Exiting.");
					return;
				}

				if (input.startsWith("read")) {
					client.readProperty(input.split(" ")[1]);
				} else if (input.startsWith("children")) {
					client.children(input.split(" ")[1]);
				} else if (input.startsWith("lookup")) {
					client.lookup(input.split(" ")[1]);
				} else {
					System.out.println("Unknown command");
				}
			}
		} finally {
			client.shutdown();
		}
	}
}
