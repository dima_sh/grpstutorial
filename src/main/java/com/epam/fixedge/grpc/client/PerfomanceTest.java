package com.epam.fixedge.grpc.client;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class PerfomanceTest {

	 public static void syncTest(KVClient client, String user) {
	        long t = System.nanoTime();
	        for (int i = 0; i < 10000; i++) { client.ping(user); }
	        System.out.println("took: " + (System.nanoTime() - t) / 1000000 + " ms");
	    }

	    public static void asyncTest(final KVClient[] client, final String user) {
	        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
	        final CountDownLatch latch = new CountDownLatch(10000);
	        long t = System.nanoTime();
	        for (int i = 0; i < 10000; i++) {
	            final int j = i;
	            pool.submit(new Runnable() {
	              //  @Override
	                public void run() {
	                    client[j % 20].ping(user);
	                    latch.countDown();
	                }
	            });
	        }
	        try {
	            latch.await();
	        }
	        catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	        System.out.println("took: " + (System.nanoTime() - t) / 1000000 + " ms");
	        pool.shutdownNow();
	    }
	    
	    public static void main(String[] args) throws Exception {
	        KVClient[] client = new KVClient[20];
	        try {
	            String user = "world";

	            int i = 0;
	            for (i = 0; i < 20; i++) {
	                client[i] = new KVClient("localhost", 50051);
	                client[i].ping(user);
	            }

	            boolean sync = true;
	            if (args.length > 0) {
	                sync = Boolean.parseBoolean(args[0]);
	            }

	            if (sync) { syncTest(client[0], user); }
	            else { asyncTest(client, user); }
	        }
	        finally {

	            for (int i = 0; i < 20; i++) {
	                client[i].shutdown();
	            }

	        }
	    }
	
}
