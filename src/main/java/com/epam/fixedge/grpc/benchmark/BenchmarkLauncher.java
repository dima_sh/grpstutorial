package com.epam.fixedge.grpc.benchmark;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;



public class BenchmarkLauncher {
	

public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PingBenchmark.class.getSimpleName())
                //.include(FlowControlledMessagesPerSecondBenchmark.class.getName() )
        		//.include(UnaryCallQpsBenchmark.class.getName())
        		.forks(1)
                .build();

        new Runner(opt).run();
}


	

}
