package com.epam.fixedge.grpc.benchmark;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.epam.fixedge.grpc.server.service.PingService;

import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import io.grpc.Server;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerServiceDefinition;
import io.grpc.ServiceDescriptor;
import io.grpc.Status;
import io.grpc.MethodDescriptor.MethodType;
import io.grpc.netty.NegotiationType;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.NettyServerBuilder;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.DefaultThreadFactory;


public abstract class AbstractBenchmark {

	private static final Logger logger = Logger.getLogger(AbstractBenchmark.class.getName());
	protected MethodDescriptor<ByteBuf, ByteBuf> unaryMethod;
	protected ByteBuf response;
	
	public enum MessageSize {
		// Max out at 1MB to avoid creating messages larger than Netty's buffer
		// pool can handle
		// by default
		SMALL(10), MEDIUM(1024), LARGE(65536), JUMBO(1048576);

		private final int bytes;

		MessageSize(int bytes) {
			this.bytes = bytes;
		}

		public int bytes() {
			return bytes;
		}
	}

	/**
	 * Standard flow-control window sizes.
	 */
	public enum FlowWindowSize {
		SMALL(16383), MEDIUM(65535), LARGE(1048575), JUMBO(8388607);

		private final int bytes;

		FlowWindowSize(int bytes) {
			this.bytes = bytes;
		}

		public int bytes() {
			return bytes;
		}
	}

	private static InetAddress buildBenchmarkAddr() {
		InetAddress tmp = null;
		try {
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			outer: while (networkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = networkInterfaces.nextElement();
				if (!networkInterface.isLoopback()) {
					continue;
				}
				Enumeration<NetworkInterface> subInterfaces = networkInterface.getSubInterfaces();
				while (subInterfaces.hasMoreElements()) {
					NetworkInterface subLoopback = subInterfaces.nextElement();
					if (subLoopback.getDisplayName().contains("benchmark")) {
						tmp = subLoopback.getInetAddresses().nextElement();
						System.out.println("\nResolved benchmark address to " + tmp + " on "
								+ subLoopback.getDisplayName() + "\n\n");
						break outer;
					}
				}
			}
		} catch (SocketException se) {
			System.out.println("\nWARNING: Error trying to resolve benchmark interface \n" + se);
		}
		if (tmp == null) {
			try {
				System.out.println("\nWARNING: Unable to resolve benchmark interface, defaulting to localhost");
				tmp = InetAddress.getLocalHost();
			} catch (UnknownHostException uhe) {
				throw new RuntimeException(uhe);
			}
		}
		return tmp;
	}

	public static void main(String[] args) throws Exception {
		System.out.println(buildBenchmarkAddr());
		ServerSocket sock = new ServerSocket();
		// Pick a port using an ephemeral socket.
		sock.bind(new InetSocketAddress(buildBenchmarkAddr(), 0));
		SocketAddress address = sock.getLocalSocketAddress();
		System.out.println(address);
	}

	protected Server server;
	// protected ByteBuf request;
	// protected ByteBuf response;
	private MethodDescriptor<ByteBuf, ByteBuf> flowControlledStreaming;
	protected ManagedChannel[] channels;

	/**
	 * Initialize the environment for the executor.
	 */
	public void setup(MessageSize requestSize, MessageSize responseSize, FlowWindowSize windowSize,
			int maxConcurrentStreams, int channelCount) throws Exception {
		NettyServerBuilder serverBuilder;
		NettyChannelBuilder channelBuilder;

		ServerSocket sock = new ServerSocket();
		// Pick a port using an ephemeral socket.
		sock.bind(new InetSocketAddress(buildBenchmarkAddr(), 0));
		SocketAddress address = sock.getLocalSocketAddress();
		sock.close();
		serverBuilder = NettyServerBuilder.forAddress(address);
		channelBuilder = NettyChannelBuilder.forAddress(address);

		/*
		 * if (serverExecutor == ExecutorType.DIRECT) {
		 * serverBuilder.directExecutor(); } if (clientExecutor ==
		 * ExecutorType.DIRECT) { channelBuilder.directExecutor(); }
		 */

		// Always use a different worker group from the client.
		ThreadFactory serverThreadFactory = new DefaultThreadFactory("STF pool", true /* daemon */);
		serverBuilder.workerEventLoopGroup(new NioEventLoopGroup(0, serverThreadFactory));

		// Always set connection and stream window size to same value
		serverBuilder.flowControlWindow(windowSize.bytes());
		channelBuilder.flowControlWindow(windowSize.bytes());

		channelBuilder.negotiationType(NegotiationType.PLAINTEXT);
		serverBuilder.maxConcurrentCallsPerConnection(maxConcurrentStreams);

		// Create buffers of the desired size for requests and responses.
		PooledByteBufAllocator alloc = PooledByteBufAllocator.DEFAULT;
		// Use a heap buffer for now, since MessageFramer doesn't know how to
		// directly convert this
		// into a WritableBuffer
		// TODO(carl-mastrangelo): convert this into a regular buffer() call.
		// See
		// https://github.com/grpc/grpc-java/issues/2062#issuecomment-234646216
		/*
		 * request = alloc.heapBuffer(requestSize.bytes());
		 * request.writerIndex(request.capacity() - 1);
		  */ 
		  response = alloc.heapBuffer(responseSize.bytes());
		  response.writerIndex(response.capacity() - 1);
		
		  unaryMethod = MethodDescriptor.create(MethodType.UNARY,
			        "benchmark/unary",
			        new ByteBufOutputMarshaller(),
			        new ByteBufOutputMarshaller());
		  serverBuilder.addService(
			        ServerServiceDefinition.builder(
			            new ServiceDescriptor("benchmark",
			                unaryMethod
			                ))
			            .addMethod(unaryMethod, new ServerCallHandler<ByteBuf, ByteBuf>() {
			                  @Override
			                  public ServerCall.Listener<ByteBuf> startCall(
			                      final ServerCall<ByteBuf, ByteBuf> call,
			                      Metadata headers) {
			                    call.sendHeaders(new Metadata());
			                    call.request(1);
			                    return new ServerCall.Listener<ByteBuf>() {
			                      @Override
			                      public void onMessage(ByteBuf message) {
			                        // no-op
			                        message.release();
			                        call.sendMessage(response.slice());
			                      }

			                      @Override
			                      public void onHalfClose() {
			                        call.close(Status.OK, new Metadata());
			                      }

			                      @Override
			                      public void onCancel() {

			                      }

			                      @Override
			                      public void onComplete() {
			                      }
			                    };
			                  }
			                })
			            
			            .build());
		// Build and start the clients and servers
		server = serverBuilder.addService(new PingService()).build();
		server.start();
		channels = new ManagedChannel[channelCount];
		ThreadFactory clientThreadFactory = new DefaultThreadFactory("CTF pool", true /* daemon */);
		for (int i = 0; i < channelCount; i++) {
			// Use a dedicated event-loop for each channel
			channels[i] = channelBuilder.eventLoopGroup(new NioEventLoopGroup(1, clientThreadFactory)).build();
		}
	}
	
	protected abstract void channelCall(ManagedChannel mchanel) ;

	protected void clientCall(int callsPerChannel) {
		for (final ManagedChannel channel : channels) {
			for (int i = 0; i < callsPerChannel; i++) {
				channelCall(channel);
			}
		}
	}

	/**
	 * Shutdown all the client channels and then shutdown the server.
	 */
	protected void teardown() throws Exception {
		logger.fine("shutting down channels");
		for (ManagedChannel channel : channels) {
			channel.shutdown();
		}
		logger.fine("shutting down server");
		server.shutdown();
		if (!server.awaitTermination(5, TimeUnit.SECONDS)) {
			logger.warning("Failed to shutdown server");
		}
		logger.fine("server shut down");
		for (ManagedChannel channel : channels) {
			if (!channel.awaitTermination(1, TimeUnit.SECONDS)) {
				logger.warning("Failed to shutdown client");
			}
		}
		logger.fine("channels shut down");
	}

}
