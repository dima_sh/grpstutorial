package com.epam.fixedge.grpc.benchmark;


import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Benchmark;


import com.epam.fixedge.adminprotocol.PingGrpc;
import com.epam.fixedge.adminprotocol.PingRequest;
import com.epam.fixedge.adminprotocol.PingResponse;

import io.grpc.CallOptions;

import io.grpc.ManagedChannel;
import io.grpc.stub.ClientCalls;
import io.grpc.stub.StreamObserver;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;


@org.openjdk.jmh.annotations.State(Scope.Benchmark)
@org.openjdk.jmh.annotations.Fork(1)
public class PingBenchmark extends AbstractBenchmark {
	
	private static AtomicLong callCounter;
	private AtomicBoolean completed;
	
	protected ByteBuf request;
	
	
	private io.grpc.MethodDescriptor<com.epam.fixedge.adminprotocol.PingRequest,
    com.epam.fixedge.adminprotocol.PingResponse> methodDescriptor;

	
	@org.openjdk.jmh.annotations.Param({"1", "5" , "10"})//, "4"})
	public int channelCount = 1;

	@org.openjdk.jmh.annotations.Param({"1", "10", "100"})//, "100"})
	public int maxConcurrentStreams = 1;
	
	//@org.openjdk.jmh.annotations.Param({ "ByteBuffer", "PingRequest"})//, "100"})
	public String messageType = "PingRequest";
	
	 /**
	   * Use an AuxCounter so we can measure that calls as they occur without consuming CPU
	   * in the benchmark method.
	   */
	  @org.openjdk.jmh.annotations.AuxCounters
	  @org.openjdk.jmh.annotations.State(Scope.Thread)
	  public static class AdditionalCounters {

	    @Setup(Level.Iteration)
	    public void clean() {
	      callCounter.set(0);
	    }

	    public long messagesPerSecond() {
	      return callCounter.get();
	    }
	  }
	  

	  /**
	   * Setup with direct executors, small payloads and the default flow-control window.
	   */
	  @Setup(Level.Trial)
	  public void setup() throws Exception {
	    super.setup(MessageSize.SMALL,
	        MessageSize.SMALL,
	        FlowWindowSize.MEDIUM,
	        maxConcurrentStreams,
	        channelCount);
	    callCounter = new AtomicLong();
	    completed = new AtomicBoolean();
		methodDescriptor = PingGrpc.METHOD_PING;
		PooledByteBufAllocator alloc = PooledByteBufAllocator.DEFAULT;
		request = alloc.heapBuffer(MessageSize.SMALL.bytes());
	    request.writerIndex(request.capacity() - 1);
	    clientCall(maxConcurrentStreams);
	  }


	@Override
	protected void channelCall(final ManagedChannel mchannel) {
	   if ("ByteBuffer".equals(messageType)) {
		   bufferedCall(mchannel);
	   } else if ("PingRequest".equals(messageType)) {
		   pingCall(mchannel);
	   }
		
	}
	
	private void bufferedCall(final ManagedChannel mchannel) {
		StreamObserver<ByteBuf> observer = new StreamObserver<ByteBuf>() {
	          @Override
	          public void onNext(ByteBuf value) {
	        	  callCounter.addAndGet(1);
	          }

	          @Override
	          public void onError(Throwable t) {
	            completed.set(true);
	          }

	          @Override
	          public void onCompleted() {
	            if (!completed.get()) {
	              ByteBuf slice = request.slice();
	              ClientCalls.asyncUnaryCall(
	                  mchannel.newCall(unaryMethod, CallOptions.DEFAULT), slice, this);
	            }
	          }
	        };
	        observer.onCompleted();
	}
	
	private void pingCall(final ManagedChannel mchannel) {
		  StreamObserver<PingResponse> observer = new StreamObserver<PingResponse>() {
	          @Override
	          public void onNext(PingResponse value) {
	        	  callCounter.addAndGet(1);
	          }

	          @Override
	          public void onError(Throwable t) {
	        	  completed.set(true);
	          }

	          @Override
	          public void onCompleted() {
	            if (!completed.get()) {
	              //ByteBuf slice = request.slice();
	              ClientCalls.asyncUnaryCall(
	                  mchannel.newCall(methodDescriptor, CallOptions.DEFAULT), 
	                  PingRequest.getDefaultInstance(), this);
	            }
	          }
	        };
	        observer.onCompleted();
	}
	
	/**
	   * Measure throughput of unary calls. The calls are already running, we just observe a counter
	   * of received responses.
	   */
	  @Benchmark
	  public void unary(AdditionalCounters counters) throws Exception {
	    // No need to do anything, just sleep here.
	    Thread.sleep(1001);
	  }
	  
	  /**
	   * Stop the running calls then stop the server and client channels.
	   */
	  @Override
	  @org.openjdk.jmh.annotations.TearDown(Level.Trial)
	  public void teardown() throws Exception {
	    completed.set(true);
	   // if (!latch.await(5, TimeUnit.SECONDS)) {
	      //logger.warning("Failed to shutdown all calls.");
	   // }

	    super.teardown();
	  }
	
	/**
	   * Useful for triggering a subset of the benchmark in a profiler.
	   */
	  public static void main(String[] argv) throws Exception {
	    PingBenchmark bench = new PingBenchmark();
	    bench.setup();
	    Thread.sleep(30000);
	    bench.teardown();
	    System.exit(0);
	  }
	
	

}
