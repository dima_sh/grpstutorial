package com.epam.fixedge.grpc.server.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.epam.fixedge.adminprotocol.KV;
import com.epam.fixedge.adminprotocol.KVList;
import com.epam.fixedge.adminprotocol.KVRequest;
import com.epam.fixedge.adminprotocol.KVServiceGrpc;

import io.grpc.stub.StreamObserver;

public class KVCRUDService extends KVServiceGrpc.KVServiceImplBase {
	
	private File propertyFile;

	public KVCRUDService(File propertyFile) {
		super();
		this.propertyFile = propertyFile;
	}

	@java.lang.Override
	public void read(com.epam.fixedge.adminprotocol.KVRequest request,
		        io.grpc.stub.StreamObserver<com.epam.fixedge.adminprotocol.KV> responseObserver) {
		try {
			Properties props = props();
			String name = request.getName();
			String value = props.getProperty(name);
			List<String> children = props.keySet().stream().filter(set -> set.toString()
					.startsWith(name)).map(e -> e.toString()).collect(Collectors.toList());
			KV reply = KV.newBuilder().setName(name).setValue((value == null? "": value))
					.addAllChildren(children).build();
			responseObserver.onNext(reply);
			
		} catch (Exception e) {
			 responseObserver.onError(e);
		} finally {
			responseObserver.onCompleted();
		}
      
	}

	@Override
	public void children(KVRequest request, StreamObserver<KVList> responseObserver) {
		try {
			Properties props = props();
			KVList.Builder reply_ = KVList.newBuilder();
			String name = request.getName();
			//reply.getPropsList().a
			props.entrySet().stream().filter(map -> map.getKey().toString().startsWith(name))
					.forEach(p -> {
				        reply_.addProps(
						        KV.newBuilder().setName(p.getKey().toString())
						        .setValue(p.getValue().toString()).build()
				        		);
				        
					});
			//KV reply = KV.newBuilder().setName(name).setValue((value == null? "": value))
		//			.addAllChildren(children).build();
			responseObserver.onNext(reply_.build());
			
			
		} catch (Exception e) {
			e.printStackTrace();
			 responseObserver.onError(e);
		} finally {
			responseObserver.onCompleted();
		}
	}
	
	private List<String> childList(Properties props,String parent) {
		if (parent == null) {
			return null;
		}
		List<String> children = props.keySet().stream().filter(set -> set.toString()
				.startsWith(parent) && set.toString().length() < parent.length()).map(e -> e.toString()).collect(Collectors.toList());
		return children;
		
	}

	@Override
	public void lookup(KVRequest request, StreamObserver<KVList> responseObserver) {
		try {
			Properties props = props();
			String wildcard = request.getName();
			KVList.Builder reply_ = KVList.newBuilder();
			props.entrySet().stream().filter(map -> 
			         map.getKey().toString().matches(convertToRegexp(wildcard)))
			.forEach(p -> {
		        reply_.addProps(
				        KV.newBuilder().setName(p.getKey().toString())
				        .setValue(p.getValue().toString())
				        .addAllChildren(childList(props, p.getKey().toString()))
				        .build());
		        
			});
		    responseObserver.onNext(reply_.build());
			
		} catch (Exception e) {
			 responseObserver.onError(e);
		} finally {
			responseObserver.onCompleted();
		}
	}
	
	private String convertToRegexp(String input) {
		if (input == null) {
			return null;
		}
		return input.replaceAll("\\.", "\\\\.").replaceAll("\\*", ".*");
	}
	
	

	@Override
	public void create(KVRequest request, StreamObserver<KV> responseObserver) {
		// TODO Auto-generated method stub
		super.create(request, responseObserver);
	}

	@Override
	public void update(KVRequest request, StreamObserver<KV> responseObserver) {
		// TODO Auto-generated method stub
		super.update(request, responseObserver);
	}

	@Override
	public void delete(KVRequest request, StreamObserver<KV> responseObserver) {
		// TODO Auto-generated method stub
		super.delete(request, responseObserver);
	}
	
	private Properties props() throws IOException {
		Properties props = new Properties();
		props.load(new FileInputStream(propertyFile));
		return props;
	}
	  
	  

}
