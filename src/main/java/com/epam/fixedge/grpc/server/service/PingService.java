package com.epam.fixedge.grpc.server.service;

import com.epam.fixedge.adminprotocol.PingGrpc;
import com.epam.fixedge.adminprotocol.PingRequest;
import com.epam.fixedge.adminprotocol.PingResponse;

import io.grpc.stub.StreamObserver;

public class PingService extends PingGrpc.PingImplBase {

	@Override
	public void ping(PingRequest request, StreamObserver<PingResponse> responseObserver) {
		PingResponse reply = PingResponse.newBuilder().setResponseId(request.getRequesId()).build();
		//System.out.println(responseObserver);
		responseObserver.onNext(reply);
		responseObserver.onCompleted();
 	}
	
	

}
