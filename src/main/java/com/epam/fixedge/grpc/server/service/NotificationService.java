package com.epam.fixedge.grpc.server.service;



import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.epam.fixedge.adminprotocol.KV;
import com.epam.fixedge.adminprotocol.NotificationGrpc;

import io.grpc.stub.StreamObserver;


public class NotificationService extends NotificationGrpc.NotificationImplBase {
	
	 private StreamObserver<com.epam.fixedge.adminprotocol.Event> observer;
     private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

	 
	 
	
	 public NotificationService() {
		super();
		executorService.scheduleAtFixedRate(new Thread() {
			 @Override
		      public void run() {
            try {
                if (observer != null) {
                	observer.onNext(com.epam.fixedge.adminprotocol.Event.getDefaultInstance()
                			.newBuilder().addEvents(KV.newBuilder().setName("event").build()).build());
                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }
		}

        }, 0, 10, TimeUnit.SECONDS);

		// TODO Auto-generated constructor stub
	}



	@java.lang.Override
	 public void subscribe(com.epam.fixedge.adminprotocol.SubscriptionRequest request,
	     io.grpc.stub.StreamObserver<com.epam.fixedge.adminprotocol.Event> responseObserver) {
		 this.observer = responseObserver;
	 }


}
